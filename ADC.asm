;************ 2017 - Fernando Victor Filippetti #93684********************
;	DPPS is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    DPPS is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with DPPS.  If not, see <http://www.gnu.org/licenses/>.
;*************************************************************************

ENABLEA2D: ;--------------------------------------------------------------
; R24 = temporal
; Z = addressing

ldi ZH, high( ADMUX )
ldi ZL, low( ADMUX )	;ref=AVCC, Ajustado a la derecha.
ldi r24, (1<<REFS0)
st Z, r24
ldi ZL, low( ADCSRB )	; cero ADCSRB
clr r24
st Z, r24
ldi ZL, low( ADCSRA )	; Enable A2D con divisor = 128
ldi r24, (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0)
st Z, r24
ret    

SETA2DCHANNEL:;------------------------------------------------------------
; R24 = chanal a setear
; R25 = uso temporal
; Z = addressing 

ldi ZH, high( ADMUX )
cbr r24, 0xF8	; enmascaro los bits que no uso    
ldi ZL,ADMUX	; leo el canal actual
ld r25, Z
cbr r25, 7	; borro los bits del mux y los piso con r24
or r25, r24
st Z, r25
ret;-----------------------------------------------------------------------

READA2D:
; R24 = return value (L)
; R25 = return value (H)
; Z = addressing

ldi ZH, 0
ldi ZL, ADCSRA
ld r24, Z	
ori r24, (1<<ADSC)	; empiezo la conversión
st Z, r24
WAIT:
ld r24, Z
sbrc r24, ADSC	;espero que termine la conv.
rjmp WAIT
ldi ZL, ADCL	;agarro el resultado
ld r24, Z
ldi ZL, ADCH	; Es necesario leer la parte alta despues.
ld r25, Z
cbr r25, 0xFC
ret


