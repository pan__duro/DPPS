;************ 2017 - Fernando Victor Filippetti #93684****************
;	DPPS is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.

;    DPPS is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.

;    You should have received a copy of the GNU General Public License
;    along with DPPS.  If not, see <http://www.gnu.org/licenses/>.

InitT0:
	sbi	DDRD, 6		; 10
	in	r24, TCCR0A	; 36 
	ori	r24, 0x83	; 128 Seteo COM0A1 Seteo WGM 0:1 en 11 
      	out	TCCR0A, r24	; 36
       	in	r24, TCCR0B	; TCCR0B
       	ori	r24, 0x01	; CS00 = 1
       	out	TCCR0B,r24	; 
       	ret
SetT0: 
       	out	OCR0A, r24	; OCR0A
  	ret


InitT2:
	SBI	DDRB,3
	LDI	XH,HIGH(TCCR2A)
	LDI	XL,LOW(TCCR2A)
	LDI	R16,0XC3	;OC2A DESHABILITADO, OC1A HABILITADO FASTPWM
	ST	X,R16
	LDI	XH,HIGH(TCCR2B)
	LDI	XL,LOW(TCCR2B)
	LDI	R16,0X02	;PRESCALER 1
	ST	X,R16
ret

SetT2:
	LDI	XH,HIGH(OCR2A)
	LDI	XL,LOW(OCR2A)
	ST	X,R24		
ret



InitT1:
	ldi	ZH,HIGH(TIMSK1)
	LDI	ZL,LOW(TIMSK1)
	ld	R24,Z
	ORI	R24,0X01
	ST	Z,R24
	sbi	ddrb,1
	sbi	ddrb,2

	LDI	R24,0XFF		;Seteo ICR1 para desbordar a tope
	ldi	ZH, 0x00	
	ldi	ZL, ICR1H
	ST	Z, r24	
	ldi	ZL, ICR1L
 	ST	Z, r24
	



	ldi	ZL,TCCR1A;______________________________	
	ldi	r24,0xA2	
	st	Z, r24
	
	ldi	ZL,TCCR1B;______________________________
	ldi	r24,0x19	
	st	Z, r24	
	ret

SetT1:
	ldi	ZH, 0x00
	ldi	ZL,OCR1BH	
	st	Z,r25	
	ldi	ZL,OCR1BL
	st	Z,r24
	ldi	ZL,OCR1AH	
	st	Z,r27	
	ldi	ZL,OCR1AL
	st	Z,r26
	ret		




TIM1_OVF: 
	cli
	push	r17		; Guardo R17 asi lo uso en la interrupcion
	in	r17,SREG 	; Guardo el SREG
	push	r17
	DEC	CONTT1
	BRNE	SALT1
		LDI	CONTT1,49		
		ORI	FLAGS,(1<<T)
SALT1:	pop	r17
	out	SREG,r17 ; Restauro el stack	
	pop	r17
	RETI





