;************ 2017 - Fernando Victor Filippetti #93684********************
;	DPPS is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    DPPS is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with DPPS.  If not, see <http://www.gnu.org/licenses/>.
;*************************************************************************

ENABLEUSART0: ;Enable USART0 at 115200 baud, 8 bits, no parity, 1 stop bit.
; R24 = temporary (various port values)
; Z = addressing for USART0 ports

	cli
	clr r24		
	ldi r31,0
	ldi r30,UBRR0H
	st Z, r24			;Set baud rate of 115200
	ldi r24, 8
	ldi r30,UBRR0L
	st Z, r24		;Configure 8-bit data, 1 stop bit, no parity
	ldi r24, (1<<UCSZ01) | (1<<UCSZ00)
	ldi r30,UCSR0C
	st Z, r24
	ldi 	r30,UCSR0B	;Habilito interrupciones Enable TX and RX
	ldi 	r24,(1 << RXCIE0) | (1 << RXEN0) | (1<<TXEN0)
	st	Z,r24
	sei
ret

sendUSART0: ;-------------------------------------------------------------
; r23 = byte to send
; r25 = temporary
; Z
	push	r25
	PUSH	ZH
	PUSH	ZL
	ldi 	ZH,0 
	ldi 	ZL,UCSR0A
LOOOOP:
	ld 	r25, Z
	sbrs 	r25, UDRE0	; Wait for an empty transmit buffer
	rjmp 	LOOOOP		;(bit gets set when empty)
	ldi 	ZL, UDR0
	st 	Z, r23              ; transmit byte
	POP	ZL
	POP	ZH
	Pop 	r25
ret

USART_RXC:
	cli
	PUSH	r16
	push	r17		; Guardo R17 asi lo uso en la interrupcion
	PUSH	R18		
	in	r17,SREG 	; Guardo el SREG
	push	r17
	PUSH	YH
	PUSH	YL
	CLR	YH 		;DIRECCIONO EL REGISTRO DE RECEPCION
	LDI	YL,UDR0			
	LD	R16,Y		;LEO EL BUFFER para limpiar la interrupción

	LDI	R17,0X0A	;cargo un "\n" en R17 se lo resto 	
	SUB	r17,r16		;a R16 para verificar si llego un enter
	BRNE	CRET
	CLR	FLAGS		;limpio todos los flags
	JMP	SALIDA		;retorno de la interrupcion.
CRET:	LDI	R17,0X0D	;cargo un "\cr" en R17	
	SUB	r17,r16		;se lo resto a R16 para verificar si llego un cr
	BRNE	ELSE1
	CLR	FLAGS		;limpio todos los flags
	JMP	SALIDA		;retorno de la interrupcion.
ELSE1:
	LDI	R17,'C'		;cargo una "C" en R17	
	SUB	r17,r16		;se lo resto a R16 para verificar si llego
	BRNE	ELSE2
	ORI	FLAGS,(1<<C)	;seteo el flago correspondiente
	JMP	SALIDA		;retorno de la interrupcion.
ELSE2:
	LDI	R17,'A'		;cargo una "A" en R17	
	SUB	r17,r16		;se lo resto a R16 para verificar si llego
	BRNE	ELSE3
	ORI	FLAGS,(1<<A)	;seteo el flago correspondiente
	JMP	SALIDA		;retorno de la interrupcion.
ELSE3:
	LDI	R17,'R'		;cargo una "R" en R17	
	SUB	r17,r16		;se lo resto a R16 para verificar si llego 
	BRNE	ELSE4
	CLR	FLAGS
	JMP	RESET		;Reseteo el micro si llegó una R !!
ELSE4:
	LDI	R17,'?'		;cargo una "?" en R17	
	SUB	r17,r16		;se lo resto a R16 para verificar si llego 
	BRNE	ELSE5
	ORI	FLAGS,(1<<HOWTO)	;seteo el flago correspondiente
	JMP	SALIDA		;retorno de la interrupcion.

ELSE5:		;Si no es ninguno de los caracteres de control lo tomo 
		;como un numero y lo guardo en la posición del contador como tal.
	ANDI	R16,0X0F	; limpio la parte alta (nibble) del dato ingresado
	MOV	R17,FLAGS	
	ANDI	R17,0X0F		;Guardo en R17 solo la
	ldi 	YL,low(RX_BUFFER) 	; posicion del contador en el Buffer
	ldi 	YH,high(RX_BUFFER)
	CLR	R18
	ADD	YL,R17 
	ADC	YH,R18
	ST 	Y,R16		; Guardo el valor en el Buffer
	INC	R17		; incremento el contador del Buffer
	ANDI	R17,0X0F	; limito el contador a 4 Bits
	ANDI	FLAGS,0XF0	; borro el contador
	OR	FLAGS,R17	; Guardo el contador de nuevo.

Salida:
	POP	YL
	POP	YH
	pop	r17
	out	SREG,r17 ; Restauro el stack
	POP	R18	
	pop	r17
	POP	R16
	sei	
	reti
	
	


		


