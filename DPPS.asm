;************ 2017 - Fernando Victor Filippetti #93684*******************
;	DPPS is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    DPPS is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with DPPS.  If not, see <http://www.gnu.org/licenses/>.
;*************************************************************************
;
;Compilacion & upload:
;avra DPPS.asm
; sudo avrdude -p m328p -c usbASP -U flash:w:DPPS.hex 
;
;Protocolo de comunicación serie:
;despues de un enter (\n) puedo mandar:
;
;- una "C" a lo cual el micro responde con 21 digitos VVVVVCCCCC-VVVVVCCCCC
; 	los cuales son el valor del ADC de tension-corriente medidos
; 	y luego los correspondientes a las salidas PWM correspondientes.
;
;- un numero de 10 digitos VVVVVCCCCC con la tensión y 
;	corriente a exponer en las salidas PWM (00000-65536)
;- una "A" a lo cual el micro responde con VVVVV,CCCCC periódicamente
;	hasta que se envíe otro comando.
;- una "R" lo cual resetea el micro.
;
;- un signo "?" a lo cual el micro responde con un 
;texto de ayuda y referencia.
;
;
; uso como registro de flags y cuenta el R3, CATDDDDD  
; donde DDDD es un espacio para contar
; la cantidad bytes de datos que llegaron por la uart. 
; t=1 CUANDO PASARON MAS DE 200 MILISEGINTOS (RESETEO el flag EN EL MAIN)

.def	FLAGS	= r20
.def	CONTT1	= r22
.define	C	7
.define	A	6
.define	T	5
.define	HOWTO	4
.define	X_OVERSAMP	64	; Tamaño del buffer Oversampling
.define	TEMP_MAX	28	; PRENDE A 55C
.define	TEMP_MIN	25	; APAGA A 50C

.INCLUDE "M328PDEF.INC"

.DSEG

				;Aca guardo el buffer de receppcion 10 NUMEROS
RX_BUFFER: 		.byte	16 
				;CADA MEDICIÓN USA DOS BYTES
OVER_CURR_BUFFER:	.byte	128;(X_OVERSAMP*2)	
OVER_VOLT_BUFFER:	.byte	128;(X_OVERSAMP*2)
OVER_X_COUNT:		.byte	1
CURR_MEASURE:		.byte	2
VOLT_MEASURE:		.byte	2

.CSEG
.ORG	0x00

jmp      RESET		; Reset
RETI
.ORG 0X1A
jmp      TIM1_OVF       ; Timer1 Overflow

RETI
RETI 
RETI
.org 0x24
jmp      USART_RXC      ; USART RX Complete

.INCLUDE "ADC.asm"
.INCLUDE "USART.asm"
.INCLUDE "TIMERS.asm"
.INCLUDE "BCD.asm"
.INCLUDE "DISPLAY.asm"



RESET:
ldi      r16,high(RAMEND)
out      SPH,r16     		; Set Stack Pointer to top of RAM
ldi      r16,low(RAMEND)
out      SPL,r16
sei      			; Enable interrupt

Setup:          ; Inicio y configuracion
	
	cli
	; Reset Watchdog Timer
	wdr
	; Start timed sequence
	lds	 r16, WDTCSR
	ori	r16, (1<<WDCE) | (1<<WDE)
	sts WDTCSR, r16
	; -- Got four cycles to set the new values from here -
	; Set new prescaler(time-out) value = 64K cycles (~0.5 s)
	ldi	r16, (1<<WDE) | (1<<WDP2) | (1<<WDP0)
	sts WDTCSR, r16
	; -- Finished setting new values, used 2 cycles -
	; Turn on global interrupt
	sei

	LDI	R16, 0xFE	   ;Configuro el PORTD
	OUT	DDRD, R16
	LDI	R16, 0xFF	   ;idem PORTB
	OUT	DDRB, R16
	CALL	enableA2D
	CALL	InitT1		;me pisa el pin del display 
	CALL	InitT2		
	CALL	enableUSART0
	CALL	InitDisplay
	LDI	R17,200
	CALL	DELAY_MS_R17
	CALL	INITCHARS
	CALL	MENSAJE_INICIO_DISPLAY
	;-----------------------------------------------------------
	LDI	R24,0	;EL CONTRASTE DEL LCD EN 0
	CALL	SetT2 		
	LDI	R16,210
LCD_UP:	INC	R24
	CALL	SetT2
	CALL	DELAY_MS
	DEC	R16
	BRNE	LCD_UP
	;-----------------------------------------------------------
	LDI	R17,200
	CALL	DELAY_MS_R17
	;-----------------------------------------------------------		
	LDI	R16,200
LCD_DOWN:DEC	R24
	CALL	SetT2
	CALL	DELAY
	DEC	R16
	BRNE	LCD_DOWN
	LDI	R24,195	;EL CONTRASTE DEL LCD EN 250
	CALL	SetT2
	;-----------------------------------------------------------
	LDI	R17,0X00	;display reset
	CALL	SEND_NIBBLE
	LDI	R17,0X01
	CALL	SEND_NIBBLE
	CALL	BUSY
	CALL	DELAY

	;LIMPIO EL BUFFER DE RECEPCIÓN
	LDI	R16,32
	ldi 	YL,low(RX_BUFFER) 	; posicion del contador en el Buffer
	ldi 	YH,high(RX_BUFFER)
	CLR	R18
LOOP_CLR:
	ST 	Y+,R18
	DEC	R16
	BRNE	LOOP_CLR

MAIN:	WDR	       	
	LDI	r24,0X02;		CARGO EL CANAL DEL LM35 EN EL ADC
	CALL	setA2Dchannel
	CALL	readA2D			;leo el ADC y lo DEJO EN R24L R25H
	LSR	R25			;ESCALO EL RESULTADO A 8BITS
	ROR	R24
	LSR	R25
	ROR	R24
	CPI	R24,TEMP_MAX
	BRLO	SIGMIN			;SI ES MENOR ME VOY SIN HACER NADA
	SBI	PORTB,4			;PRENDO EL COOLER
	JMP	SIG0
SIGMIN:	CPI	R24,TEMP_MIN
	BRSH	SIG0
	CBI	PORTB,4			;APAGO EL COOLER
SIG0:
	MOV	R21,FLAGS
	ANDI	R21,(1<<C)
	BREQ	SIG1
		ANDI		FLAGS,~(1<<C)
		LDI	r24,0X00 ;		CARGO EL CANAL A SETEAR EN EL ADC
		CALL	setA2Dchannel
		CALL	readA2D			;leo el ADC y lo DEJO EN R23
		MOV	R17,R25 ;LEVANTO LOS VALORES DEL adc
		MOV	R16,R24				
		call	Enviarnumero
		;LDI	R23,','
		;CALL	sendusart0
		LDI	r24,0X01 ;		CARGO EL CANAL A SETEAR EN EL ADC
		CALL	setA2Dchannel
		CALL	readA2D			;leo el ADC y lo DEJO EN R23
		MOV	R17,R25 	;LEVANTO LOS VALORES DEL adc
		MOV	R16,R24					
		call	Enviarnumero
		ldi	ZH, 0x00
		ldi	ZL,OCR1BH	
		LD	R17,Z	
		ldi	ZL,OCR1BL
		LD	R16,Z
		call	Enviarnumero
		ldi	ZL,OCR1AH	
		LD	R17,Z	
		ldi	ZL,OCR1AL
		LD	R16,Z
		call	Enviarnumero
		ldi	r23,0x0A
		CALL	sendUSART0 ;ENVIO R23
SIG1:	MOV	R21,FLAGS
	ANDI	R21,(1<<HOWTO)
	BREQ	SIG2
		CALL	Enviar_Version
		ANDI	FLAGS,~((1<<HOWTO)|(1<<C)|(1<<A)|(1<<T))
SIG2:		
	MOV	R21,FLAGS
	ANDI	R21,(1<<T)
	BREQ	SIG3
		ANDI	FLAGS,~(1<<T)
		PUSH	R20
		CALL	READ_VOLT_OVERSAMPLING	
		CALL	READ_CURR_OVERSAMPLING	
		CALL	DISPLAY_REFRESH;
		CALL	DAC_REFRESH		;ACTUALIZA LOS VALORES DEL DAC 
						;EN BASE AL BUFFERQUE SE LLENA POR rs232
		POP	R20				
		MOV	R21,FLAGS
		ANDI	R21,(1<<A)
		BREQ	SIG3  ;Pregunto si A esta activo. Si es asi envio el dato.
			LDI	YL,LOW(VOLT_MEASURE)
			LDI 	YH,high(VOLT_MEASURE)
			LD	R17,Y+ ;LEVANTO LOS VALORES DE TENSION QUE PUSE EN RAM 
			LD	R16,Y					
			call	Enviarnumero
			LDI	R23,','
			CALL	sendusart0
			LDI	YL,LOW(CURR_MEASURE)
			LDI 	YH,high(CURR_MEASURE)
			LD	R17,Y+ ;LEVANTO LOS VALORES DE TENSION QUE PUSE EN RAM 
			LD	R16,Y					
			call	Enviarnumero
			ldi	r23,0x0A
			CALL	sendUSART0 ;ENVIO R23
SIG3:
	CALL	UPDATE_ADC	
	SLEEP
	RJMP	MAIN           			;Repite indefinidamente el main

DAC_REFRESH:
	PUSH	r16	;UTILIZA EL BUFFER DE ENTRADA EN ASCII Y 
	PUSH	r17	;SETEA LAS SALIDAS PWM HACIENDO EL TRASPASO BCD-BINARIO
	push	r19
	PUSH	YH
	PUSH	YL
	CLR	R16
	CLR	R17
	ldi 	YL,low(RX_BUFFER)  
	ldi 	YH,high(RX_BUFFER)
	LD	R19,Y+
	MOV	R18,R19
	LD	R19,Y+
	SWAP	R19
	OR	R17,R19
	LD	R19,Y+
	OR	R17,R19
	LD	R19,Y+
	SWAP	R19
	OR	R16,R19
	LD	R19,Y+
	OR	R16,R19
	;r14	;Low byte of binary result (same as mp10L)
	;r15	;High byte of binary result (same as mp10H)
	;r16	;BCD value digits 1 and 0
	;r17	;BCD value digits 2 and 3
	;r18	;BCD value digit 5
	CALL	BCD2bin16		; - BCD to 16-Bit Binary Conversion
	MOV	R24,R14 		;PONGO LOS PRIMEROS 5 DATOS EN EL DAC
	MOV	R25,R15
	CLR	R16			;SIGO CON LOS OTROS 5 NUMEROS
	CLR	R17
	LD	R19,Y+
	MOV	R18,R19
	LD	R19,Y+
	SWAP	R19
	OR	R17,R19
	LD	R19,Y+
	OR	R17,R19
	LD	R19,Y+
	SWAP	R19
	OR	R16,R19
	LD	R19,Y+
	OR	R16,R19
	;r14	;Low byte of binary result (same as mp10L)
	;r15	;High byte of binary result (same as mp10H)
	;r16	;BCD value digits 1 and 0
	;r17	;BCD value digits 2 and 3
	;r18	;BCD value digit 5
	CALL	BCD2bin16		; - BCD to 16-Bit Binary Conversion
	MOV	R26,R14 		;PONGO LOS SEGUNDOS 5 DATOS EN EL DAC
	MOV	R27,R15
	CALL	SetT1;
	POP	YL
	POP	YH
	pop	r19
	pop	r17
	POP	R16
RET	

READ_CURR_OVERSAMPLING:			;DEVUELVO EL RESULTADO EN R24,R25 L,H
	PUSH	r16
	push	r17			;Guardo R17 asi lo uso en la interrupcion
	push	r18
	push	r19		
	PUSH	YH
	PUSH	YL
	CLR	R24
	CLR	R25
	LDI	R16,X_OVERSAMP
	ldi 	YL,low(OVER_CURR_BUFFER) 
	ldi 	YH,high(OVER_CURR_BUFFER) 
LOOP_CURR_OVER:	
	LD	R18,Y+
	LD	R17,Y+
	ADD	R24,R17
	ADC	R25,R18
	DEC	R16		
	BRNE	LOOP_CURR_OVER	
	LSR	R25
	ROR	R24
	LSR	R25
	ROR	R24
	LSR	R25
	ROR	R24
	ldi 	YH,high(CURR_MEASURE) 		
	ldi 	YL,low(CURR_MEASURE) 
	ST	Y+,R25
	ST	Y,R24
	POP	YL
	POP	YH
	POP	R19
	POP	R18
	pop	r17
	POP	R16
RET

READ_VOLT_OVERSAMPLING:		;DEVUELVO EL RESULTADO EN R24,R25 L,H
	PUSH	r16
	push	r17		;Guardo R17 asi lo uso en la interrupcion
	push	r18
	push	r19	
	push	r25
	push	r24	
	PUSH	YH
	PUSH	YL
	CLR	R24
	CLR	R25
	LDI	R16,X_OVERSAMP
	ldi 	YL,low(OVER_VOLT_BUFFER) 
	ldi 	YH,high(OVER_VOLT_BUFFER) 
LOOP_VOLT_OVER:
	LD	R18,Y+
	LD	R17,Y+
	ADD	R24,R17
	ADC	R25,R18
	DEC	R16		
	BRNE	LOOP_VOLT_OVER	
	LSR	R25
	ROR	R24
	LSR	R25
	ROR	R24
	LSR	R25
	ROR	R24
	ldi 	YH,high(VOLT_MEASURE) 
	ldi 	YL,low(VOLT_MEASURE)  
	ST	Y+,R25
	ST	Y,R24
	POP	YL
	POP	YH
	POP	R24
	POP	R25
	POP	R19
	POP	R18
	pop	r17
	POP	R16
RET

UPDATE_ADC:
	PUSH	r16
	push	r17		
	PUSH	YH
	PUSH	YL
	PUSH	R24
	CLR	R16
	ldi 	YL,low(OVER_X_COUNT) 	
	ldi 	YH,high(OVER_X_COUNT) 
	LD	R17,Y
	INC	R17
	ANDI	R17,0x3F 	;LIMITO LA CUENTA A 0X3F
				;( OVER_X_COUNT TIENE QUE SER 2 ELEVADO A LA N)
	ST	Y,R17			;GUARDO DE NUEVO EL CONTADOR
	ldi 	YL,low(OVER_CURR_BUFFER) 
	ldi 	YH,high(OVER_CURR_BUFFER) 
	LSL	R17			;MULTIPLICO POR 2 R17 PARA DIRECCIONAR DE A DOS
	ADD	YL,R17			;APUNTO AL BYTE N PARA ESCRIBIR
	ADC	YH,R16
	LDI	r24,0X00 ;		CARGO EL CANAL A SETEAR EN EL ADC
	CALL	setA2Dchannel
	CALL	readA2D			;leo el ADC y lo DEJO EN R23
	ST	Y+,R25	;(H)
	ST	Y,R24	;(L)
	ldi 	YL,low(OVER_VOLT_BUFFER)
	ldi 	YH,high(OVER_VOLT_BUFFER)
	ADD	YL,R17			;APUNTO AL BYTE N PARA ESCRIBIR
	ADC	YH,R16
	LDI	r24,0X01 		;CARGO EL CANAL A SETEAR EN EL ADC
	CALL	setA2Dchannel
	CALL	readA2D			;leo el ADC y lo DEJO EN R23
	ST	Y+,R25 	;(H)
	ST	Y,R24	;(L)
	POP	R24
	POP	YL
	POP	YH
	pop	r17
	POP	R16
RET	

EnviarNumero:
	;envio lo que hay en r24 (L) y r25 (H) con 5 digitos.
	CALL	bin2BCD16
	MOV	R23,R15
	ORI	R23,0X30
	CALL	sendUSART0 ;ENVIO R23
	MOV	R23,R14 
	SWAP	R23
	ANDI	R23,0X0F
	ORI	R23,0X30
	CALL	sendUSART0 ;ENVIO R23
	MOV	R23,R14
	ANDI	R23,0X0F
	ORI	R23,0X30
	CALL	sendUSART0 ;ENVIO R23
	MOV	R23,R13
	SWAP	R23
	ANDI	R23,0X0F
	ORI	R23,0X30                  
	CALL	sendUSART0 ;ENVIO R23
	MOV	R23,R13
	ANDI	R23,0X0F
	ORI	R23,0X30
	CALL	sendUSART0 ;ENVIO R23
	RET

Enviar_Version:
	Ldi 	R16,0xAB;
	Ldi 	R17,0x02;
	LDI	ZH,HIGH(TABLA<<1)
	LDI	ZL,LOW(TABLA<<1)
LOOPTX:
	LPM	R23,Z+
	CALL	sendUSART0 ;ENVIO R23		
	dec	R16
	brne	LOOPTX		;salta si no llego a cero.
	dec	R17	
	brne	LOOPTX
	ldi	r23,0x0A	;manda el caracter de fin de linea
	CALL	sendUSART0 	;ENVIO R23
	ret

TABLA:	
.db	"___Digital Programable Power Supply___ ",0x0A
.db	" (C) Fernando V. Filippetti",0X0A
.db	" Release date %DAY%/%MONTH%/%YEAR% V1.0",0X0A
.db	"Comandos:",0X0A
.db	"C  responde con 21 digitos VVVVVCCCCCVVVVVCCCCC los cuales son",0X0A
.db	" el valor del ADC de tension-corriente medidos ",0X0A
.db	"seguido las tensiones y corrientes target                ",0x0A
.db	"A responde con VVVVV,CCCCC periódicamente hasta que se envíe otro comando.",0x0A
.db	"R resetea la fuente",0X0A
.db	"? muestra este mensaje de ayuda y referencia.   ",0X0A

ESPACIO:
	PUSH	R17
	ldi	r17,32
	CALL	WRITECHAR ;MUESTRA R17
	POP	R17
RET

DISPLAY_REFRESH:
	LDI	R17,0X02
	CALL	SETCURSOR
	LDI	YL,LOW(VOLT_MEASURE)
	LDI 	YH,high(VOLT_MEASURE)
	LD	R17,Y+ ;LEVANTO LOS VALORES DE TENSION QUE PUSE EN RAM 
	LD	R16,Y
	LSR	R17	;divido el valor por 4, maximo: 20.46 V
	ROR	R16
	LSR	R17	
	ROR	R16
	CALL	bin2BCD16
	MOV	R17,R14 
	SWAP	R17
	ANDI	R17,0X0F
	BRNE	VOLTA
	CALL	ESPACIO
	RJMP	VOLTB
VOLTA:	ORI	R17,0X30
	CALL	WRITECHAR
VOLTB:	MOV	R17,R14
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	LDI	R17,1 ; ESTO ES UNA COMA
	CALL	WRITECHAR
	MOV	R17,R13
	SWAP	R17
	ANDI	R17,0X0F
	ORI	R17,0X30                  
	CALL	WRITECHAR
	MOV	R17,R13
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	LDI	R17,'V'
	CALL	WRITECHAR
	
	LDI	R17,0X0A	;Aca muestra la corriente
	CALL	SETCURSOR
	LDI	YL,LOW(CURR_MEASURE)
	LDI 	YH,HIGH(CURR_MEASURE)
	LD	R17,Y+ 	;LEVANTO LOS VALORES DE corriente QUE PUSE EN RAM 
	LD	R16,Y
	LSR	R17	;divido el valor por dos, maximo: 4.092 A
	ROR	R16
	CALL	bin2BCD16
	MOV	R17,R14 
	SWAP	R17
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	PUSH	R17
	LDI	R17,1 ; ESTO ES UNA COMA
	CALL	WRITECHAR
	POP	R17
	MOV	R17,R14
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	MOV	R17,R13
	SWAP	R17
	ANDI	R17,0X0F
	ORI	R17,0X30                  
	CALL	WRITECHAR
	MOV	R17,R13
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	LDI	R17,'A'
	CALL	WRITECHAR

	LDI	R17,0X40	;aca tiene que mostrar la IMPEDANCIA
	CALL	SETCURSOR
	LDI	YL,LOW(CURR_MEASURE)
	LDI 	YH,HIGH(CURR_MEASURE)
	LD	R19,Y+ ;LEVANTO LOS VALORES DE corriente QUE PUSE EN RAM 
	LD	R18,Y
	CPI	R19,0		;CHEQUEO CORRIENTE DISTINTA DE 0!!
	BRNE	MOSTAR_IMP
	CPI	R18,0		;CHEQUEO CORRIENTE DISTINTA DE 0!!
	BRNE	MOSTAR_IMP
	LDI	R17,0X40	
	CALL	SETCURSOR
	LDI	R17,' '
	CALL	WRITECHAR
	CALL	WRITECHAR
	CALL	WRITECHAR
	CALL	WRITECHAR
	CALL	WRITECHAR
	CALL	WRITECHAR
	LDI	R17,'-'
	CALL	WRITECHAR
	LDI	R17,' '	
	CALL	WRITECHAR
	JMP	SAL_IMP	
MOSTAR_IMP:
	LDI	YL,LOW(VOLT_MEASURE)
	LDI 	YH,high(VOLT_MEASURE)
	LD	R17,Y+ ;LEVANTO LOS VALORES DE TENSION QUE PUSE EN RAM 
	LD	R16,Y
	MOV	R14,R16	;multiplico el valor por 5, por que es la tension
	MOV	R15,R17
	LSL	R16	
	ROL	R17
	LSL	R16	
	ROL	R17
	ADD	R14,R16
	ADC	R15,R17
	MOV	R16,R14	; GUARDO EL RESULTADO EN LAS VARIABLES ORIGINALES
	MOV	R17,R15
	call	div16u
	PUSH	R14	;GUARDO EN STACK EL resto DE LA DIVISIÓN 
	PUSH	R15	;PARA USARLO DESPUES DE MOSTRAR LA PARTE ENTERA.
	CALL	bin2BCD16
	MOV	R17,R15
	ORI	R17,0X30
	CPI	R17,0X30
	BRNE	ZCAR_A	
	LDI	R17,32
ZCAR_A:	CALL	WRITECHAR	;MUESTRA R17
	MOV	R16,R17
	MOV	R17,R14 
	SWAP	R17
	ANDI	R17,0X0F
	ORI	R17,0X30
	CPI	R16,32
	BRNE	ZCAR_B
	CPI	R17,0X30
	BRNE	ZCAR_B	
	LDI	R17,32
	MOV	R16,R17
ZCAR_B:	CALL	WRITECHAR
	MOV	R16,R17
	MOV	R17,R14
	ANDI	R17,0X0F
	ORI	R17,0X30
	CPI	R16,32
	BRNE	ZCAR_C
	CPI	R17,0X30
	BRNE	ZCAR_C	
	LDI	R17,32
	MOV	R16,R17
ZCAR_C:	CALL	WRITECHAR
	MOV	R16,R17
	MOV	R17,R13
	SWAP	R17
	ANDI	R17,0X0F
	ORI	R17,0X30  
	CPI	R16,32
	BRNE	ZCAR_D
	CPI	R17,0X30
	BRNE	ZCAR_D	
	LDI	R17,32
ZCAR_D:	CALL	WRITECHAR
	MOV	R16,R17
	MOV	R17,R13
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	PUSH	R17
	LDI	R17,1 ; ESTO ES UNA COMA
	CALL	WRITECHAR
	POP	R17
	POP	R15
	POP	R14
	CLR	R16 ;BORRO LOS REGISTROS AUXILIARES
	CLR	R17
	LSL	R14 ;MULTIPLICO POR 2	
	ROL	R15
	ADD	R16,R14 ;LO SUMO EN EL AUXILIAR
	ADC	R17,R15
	LSL	R14	;MULTIPLICO POR 4 MAS (EN TOTAL ACA ES X8)
	ROL	R15
	LSL	R14	
	ROL	R15
	ADD	R16,R14 ;LO SUMO EN EL AUXILIAR (2+8  VECES LO QUE TENIA, MULTIPLIQUE POR 10)
	ADC	R17,R15
	LDI	YL,LOW(CURR_MEASURE)
	LDI 	YH,HIGH(CURR_MEASURE)
	LD	R19,Y+ ;LEVANTO LOS VALORES DE corriente QUE PUSE EN RAM DE NUEVO A LOS REGISTROS P/DIVIDIR
	LD	R18,Y
	CALL	div16u ; DIVIDO EL RESTO DE LA ANTERIOR PERO MULTIPLICADO POR 10 PARA QUE ME DE EL PRIMER DECIMAL.
	MOV	R17,R16
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	CLR	R16 ;BORRO LOS REGISTROS AUXILIARES
	CLR	R17
	LSL	R14 ;MULTIPLICO POR 2	
	ROL	R15
	ADD	R16,R14 ;LO SUMO EN EL AUXILIAR
	ADC	R17,R15
	LSL	R14	;MULTIPLICO POR 4 MAS (EN TOTAL ACA ES X8)
	ROL	R15
	LSL	R14	
	ROL	R15
	ADD	R16,R14 ;LO SUMO EN EL AUXILIAR (2+8  VECES LO QUE TENIA, MULTIPLIQUE POR 10)
	ADC	R17,R15
	LDI	YL,LOW(CURR_MEASURE)
	LDI 	YH,HIGH(CURR_MEASURE)
	LD	R19,Y+ ;LEVANTO LOS VALORES DE corriente QUE PUSE EN RAM DE NUEVO A LOS REGISTROS P/DIVIDIR
	LD	R18,Y
	CALL	div16u ; DIVIDO EL RESTO DE LA ANTERIOR PERO MULTIPLICADO POR 10 PARA QUE ME DE EL PRIMER DECIMAL.
	MOV	R17,R16
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
SAL_IMP:
	PUSH	R17
	LDI	R17,0
	CALL	WRITECHAR
	POP	R17
	

	LDI		R17,0X4A	;aca tiene que mostrar la POTENCIA
	CALL		SETCURSOR	
	LDI	YL,LOW(CURR_MEASURE)
	LDI 	YH,HIGH(CURR_MEASURE)
	LD	R19,Y+ ;LEVANTO LOS VALORES DE corriente QUE PUSE EN RAM 
	LD	R18,Y
	LDI	YL,LOW(VOLT_MEASURE)
	LDI 	YH,high(VOLT_MEASURE)
	LD	R17,Y+ ;LEVANTO LOS VALORES DE TENSION QUE PUSE EN RAM 
	LD	R16,Y
	LSL	R16	; multiplico la tensión por dos
	ROL	R17
	LSL	R18	; multiplico la corriente por 4 asi termino de ajustar la escala.
	ROL	R19
	LSL	R18	
	ROL	R19
	Call	mpy16u
	MOV	R16,R20
	MOV	R17,R21
	CALL	bin2BCD16
	MOV	R17,R15
	ANDI	R17,0X0F
	MOV	R17,R14 
	SWAP	R17
	ANDI	R17,0X0F
	BRNE	WATT1
	CALL	ESPACIO
	RJMP	WATT2		;--------Descomentar esto cuando encuentre el BUG de AVRA
WATT1:	ORI	R17,0X30
	CALL	WRITECHAR
WATT2:	MOV	R17,R14		;--------Descomentar esto cuando encuentre el BUG de AVRA
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	PUSH	R17
	LDI	R17,1 ; ESTO ES UNA COMA
	CALL	WRITECHAR
	POP	R17
	MOV	R17,R13
	SWAP	R17
	ANDI	R17,0X0F
	ORI	R17,0X30                  
	CALL	WRITECHAR
	MOV	R17,R13
	ANDI	R17,0X0F
	ORI	R17,0X30
	CALL	WRITECHAR
	LDI	R17,'W'
	CALL	WRITECHAR
RET

MENSAJE_INICIO_DISPLAY:
	PUSH	R16
	PUSH	R17
	PUSH	ZH
	PUSH	ZL
	LDI	R17,0X00
	LDI	R16,0X4F
	CALL	SETCURSOR	
	LDI	ZH,HIGH(INFOINICIO<<1)
	LDI	ZL,LOW(INFOINICIO<<1)
LOOP_PRINT:
	LPM	R17,Z+
	CALL	WRITECHAR ;ESCRIBO R17		
	dec	R16
	brne	LOOP_PRINT		;salta si no llego a cero.
	POP	ZL
	POP	ZH
	POP	R17
	POP	R16
	ret

INFOINICIO:
.db	" << DPPS v1.0 >>@@@@@@@@@@@@@@@@@@@@@@@@" 
.db	"Build:%DAY%/%MONTH%/%YEAR%"

DELAY_MS:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 16 000 cycles
; 1ms at 16 MHz

    ldi  r18, 21
    ldi  r19, 199
L1: dec  r19
    brne L1
    dec  r18
    brne L1
ret

DELAY:	WDR
	PUSH	R17
	LDI	R17, 5
LOOP1:	CALL	DELAY_MS
	DEC	R17
	BRNE	LOOP1
	POP	R17
	RET	

DELAY_MS_R17:
	WDR	
	PUSH	R18
	PUSH	R19
LOOP2:	CALL	DELAY
	DEC	R17
	BRNE	LOOP2
	POP	R19
	POP	R18
	RET	
	








